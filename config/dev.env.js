'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
    NODE_ENV: '"development"',
   // BASE_API: '" https://easy-mock.com/mock/5ab1def9a93c6716f0951b51/bt/menu/newbase"',
})