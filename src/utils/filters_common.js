import { transformTime } from '@/utils/common'

// 把时间撮转换成一般的时间：timeFormatter  /*"yyyy-mm-dd hh:ii:ss": 2015-11-05 17:39:23     "yyyy-mm-dd": 2015-11-05    "yyyy/mm/dd hh/ii/ss":2015/11/05 17/39/23  "yyyy/mm/dd":2015/11/05*/
export function transformTimeF(time, timeFormatter) {
  var formatter = 'yyyy-mm-dd hh:ii:ss'
  if (timeFormatter) formatter = timeFormatter
  var formatterTime = ''
  if (time) {
    var timev = new Date(parseInt(time))
    formatterTime = transformTime(formatter, timev)
  }
  return formatterTime
}
// 把时间撮转换时间：timeFormatter  /*"yyyy-mm-dd ": 2015-11-05
export function transformTimeYMD(time, timeFormatter) {
  var formatter = 'yyyy-mm-dd'
  if (timeFormatter) formatter = timeFormatter
  var formatterTime = ''
  if (time) {
    var timev = new Date(parseInt(time))
    formatterTime = transformTime(formatter, timev)
  }
  return formatterTime
}

// 时间字符串转换为一般时间格式  例如20180310151342
export function changeTimeFormat(timeText) {
  let trans_time = String(timeText)
  const trans_time_year = trans_time.substr(0, 4)
  const trans_time_month = trans_time.substr(4, 2)
  const trans_time_date = trans_time.substr(6, 2)
  const trans_time_hour = trans_time.substr(8, 2)
  const trans_time_minutes = trans_time.substr(10, 2)
  const trans_time_seconds = trans_time.substr(12, 2)
  trans_time = trans_time_year + '-' + trans_time_month + '-' + trans_time_date + ' ' + trans_time_hour + ':' + trans_time_minutes + ':' + trans_time_seconds
  return trans_time
}

// 状态商户的审核状态判断
export function judgeAuditingStatusF(status) {
  const statusList = {
    '00': '审核已通过',
    '03': '审核未提交',
    '05': '审核中',
    '07': '审核未通过',
    '99': '已删除',
    '09': '禁用'
  }
  return statusList[status]
}
// 机构操作行为判断
export function judgeOptionOrganNameF(status) {
  const statusList = {
    'ADD': '新增',
    'UPDATE': '修改',
    'DELETE': '删除',
    'APPLY': '提交审核',
    'PASS': '审核通过',
    'NOPASS': '审核未通过',
    'PAUSE': '停用',
    'RECOERY': '启用'
  }
  return statusList[status]
}
// 数据库分为单位(100=1元) 金钱,保留两位小数
export function moneyFormatter(money) {
  let moneyv
  if (money) {
    moneyv = (parseInt(money) * 100 / 10000).toFixed(2)
  }
  if (parseInt(money) === 0) {
    return 0
  }
  return moneyv
}

// 订单状态判断
export function orderStatusF(status) {
  const statusList = {
    '02': '处理中',
    '03': '成功',
    '04': '失败',
    '05': '中止'
  }
  return statusList[status]
}
