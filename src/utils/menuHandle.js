
function addChildrens(childrenArray, childData) {
  for (var n = 0, m = childData.length; n < m; n++) {
    var theData2 = {
      'hidden': false,
      'noDropdown': false
    }
    theData2.path = childData[n].menuPath
    theData2.name = childData[n].menuName
    theData2.meta = {}
    theData2.meta.title = childData[n].menuName
    theData2.meta.icon = childData[n].menuIcon
    theData2.id = childData[n].id
    theData2.menuParentId = childData[n].menuParentId
    if (childData[n].children) {
      theData2.children = []
      addChildrens(theData2.children, childData[n].children)
    }
    childrenArray.push(theData2)
  }
}

function handleData(menuData) {
  var hdata = []
  addChildrens(hdata, menuData)
  return hdata
}

export {
  handleData
}
