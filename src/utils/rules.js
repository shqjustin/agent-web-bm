var QQV = (rule, value, callback) => {
  debugger
  if (value === '') {
    callback(new Error('输入QQ号'))
  } else if (/^[1-9][0-9]{4,10}$/.test(value)) {
    callback()
  } else {
    callback(new Error('输入正确的QQ号'))
  }
}

// 类似金钱,首位不为0,最多2位小数
export function checkNumPot2(rule, value, callback) {
  const reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/
  if (value === '') {
    return callback(new Error('请填写数字'))
  } else if (!reg.test(value)) {
    return callback(new Error('请填写数字,最多2位小数'))
  } else {
    callback()
  }
}

// 身份证
export function checkIdNum(rule, value, callback) {
  const reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/
  if (!value) {
    return callback(new Error('证件号码不能为空'))
  } else if (!reg.test(value)) {
    return callback(new Error('证件号码不正确'))
  } else {
    callback()
  }
}

// 整数
export function checkInterNum(rule, value, callback) {
  // const reg = /^[0-9]*[1-9][0-9]$/
  // const reg = /^[0-9]*$/
  const reg = /^\d+$/
  if (value === '') {
    return callback(new Error('不能为空'))
  } else if (!reg.test(value)) {
    return callback(new Error('请输入整数格式'))
  } else {
    callback()
  }
}

// 请输入手机串号
export function checkPhone(rule, value, callback) {
  const reg = /^1[34578]\d{9}$/
  if (!value) {
    return callback(new Error('请输入手机串号'))
  } else if (!reg.test(parseInt(value))) {
    return callback(new Error('请输入正确的手机串号'))
  } else {
    callback()
  }
}

export default {
  QQ: [{ required: true, validator: QQV, trigger: 'blur' }],
  phone: [{ required: true, pattern: /^1[34578]\d{9}$/, message: '目前只支持中国大陆的手机号码', trigger: 'blur' }],
  roleName: [{ required: true, message: '角色名不能为空' }],
  roleBelong: [{ required: true, message: '所属产品不能为空' }],
  email: [{ required: true, pattern: /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/, message: '请输入正确邮箱', trigger: 'blur' }],
  sfzId: [{ required: true, pattern: /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/, message: '请输入正确18位身份证', trigger: 'blur' }],
  Plate: [{ required: true, pattern: /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-Z0-9]{4}[A-Z0-9挂学警港澳]{1}$/, message: '车牌号错误', trigger: 'blur' }],
  channelType: [{ required: true, message: '请选择支付类型' }],
  businessType: [{ required: true, message: '请选择业务类型' }],
  vendorName: [{ required: true, message: '请输入供应商名字' }],
  vendorCode: [{ required: true, message: '请输入供应商代码' }],
  channelName: [{ required: true, message: '请输渠道名字' }],
  microserviceName: [{ required: true, message: '请微服务名字' }],
  microserviceVersion: [{ required: true, message: '请微服务版本号' }],
  numPot2: [{ required: true, validator: checkNumPot2, trigger: 'blur' }],
  InterNum: [{ required: true, validator: checkInterNum, trigger: 'blur' }],
  word: [{ required: true, message: '不能为空', trigger: 'blur' },
    { min: 1, max: 30, message: '长度在 1 到 30 个字符', trigger: 'blur' }],
  creditCardNum: [{ required: true, pattern: /^([1-9]{1})(\d{12,18})$/, message: '银行卡位数错误,13到19位', trigger: 'blur' }]
}
