import { getToken } from '@/utils/auth'
// 给每个请求加上 token 和 activeUrlId
export function addParams(params) {
    var uploadParams = {
        'token': getToken(),
        'activeUrlId': '1000000',
        'time': (new Date()).getTime()
    }
    if (params) {
        for (const key in params) {
            uploadParams[key] = params[key]
        }
    }
    return uploadParams
}

/* 克隆对象 */
export function cloneObj(obj) {
    var str, newobj = obj.constructor === Array ? [] : {}
    if (typeof obj !== 'object') {
        return
    } else if (window.JSON) {
        str = JSON.stringify(obj), // 序列化对象
            newobj = JSON.parse(str) // 还原
    } else {
        for (var i in obj) {
            newobj[i] = typeof obj[i] === 'object' ? cloneObj(obj[i]) : obj[i]
        }
    }
    return newobj
}

// 深拷贝
export function deepCopy(obj) {
    const result = {}
    for (const key in obj) {
        result[key] = obj[key]
    }
    return result
}
// 清空之字符串的前后空格,中间的多个空格换成一个空格
export function TrimContent(str) {
    if (typeof(str) === 'string') {
        str = str.replace(/(^\s*)|(\s*$)/g, '')
        str = str.replace(/\s+/g, ' ')
    }
    return str
}
// 清空之字符串的前后空格
export function Trim(str) {
    if (typeof(str) === 'string') {
        str = str.replace(/(^\s*)|(\s*$)/g, '')
    }
    return str
}
// 清空之字符串的前后空格
export function myTrim(str) {
    if (typeof(str) === 'string') {
        str = str.replace(/(^\s*)|(\s*$)/g, '')
    }
    return str
}
// 清空对象中属性值为空的属性
export function clearNull(obj) {
    for (const key in obj) {
        obj[key] = myTrim(obj[key])
        if (obj[key] === '' || obj[key] === undefined || obj[key] === null || obj[key].length === 0) {
            delete obj[key]
        } else {
            obj[key] = obj[key]
        }
    }
    return obj
}
// 清空弹出框,清空一个对象
export function clearDialog(obj) {
    for (const key in obj) {
        obj[key] = null
    }
}
// 延时期 指令 指令值
export function delayer(directiveName, val, time) {
    setTimeout(() => {
        directiveName = val
    }, time)
}
// 关闭弹出框  （关闭弹出框指令,清空弹出框数据对象名字）
export function closeDialog(closeOrder, name) {
    setTimeout(() => {
        closeOrder = false
        clearDialog(name) // 清空弹出框
    }, 400)
}

/*
格式化时间
format :"yyyy-mm-dd hh:ii:ss";

  return 2015-11-05 17:39:23
format :"yyyy-mm-dd";

    return 2015-11-05
format :"yyyy/mm/dd hh/ii/ss";

    return 2015/11/05 17/39/23
format :"yyyy/mm/dd";

  return 2015/11/05

 */
// 时间撮转换为一般时间格式  （时间撮）
export function transformTime(formart, newDate) {
    var dateSty = arguments[0].split(' '),
        separatorFirst = '',
        separatorSecond = '',
        realDate = ''
    var yyyy = newDate.getFullYear(),
        mm = newDate.getMonth() + 1,
        dd = newDate.getDate(),
        hh = newDate.getHours(),
        ii = newDate.getMinutes(),
        ss = newDate.getSeconds()
    mm = mm < 10 ? '0' + mm : mm
    dd = dd < 10 ? '0' + dd : dd
    hh = hh < 10 ? '0' + hh : hh
    ii = ii < 10 ? '0' + ii : ii
    ss = ss < 10 ? '0' + ss : ss

    if (dateSty.length > 1) {
        separatorFirst = dateSty[0].charAt(4)
        separatorSecond = dateSty[1].charAt(2)
        realDate = yyyy + separatorFirst + mm + separatorFirst + dd + ' ' + hh + separatorSecond + ii + separatorSecond + ss
    } else {
        separatorFirst = dateSty[0].charAt(4)
        realDate = yyyy + separatorFirst + mm + separatorFirst + dd
    }

    return realDate
}

// 时间字符串转换为一般时间格式  例如20180310151342
export function changeTimeFormat(timeText) {
    let trans_time = String(timeText)
    let trans_time_year = trans_time.substr(0, 4)
    let trans_time_month = trans_time.substr(4, 2)
    let trans_time_date = trans_time.substr(6, 2)
    let trans_time_hour = trans_time.substr(8, 2)
    let trans_time_minutes = trans_time.substr(10, 2)
    let trans_time_seconds = trans_time.substr(12, 2)
    trans_time = trans_time_year + "-" + trans_time_month + "-" + trans_time_date + " " + trans_time_hour + ":" + trans_time_minutes + ":" + trans_time_seconds;
    return trans_time;
}

// 判断类型的方法  数组，对象属性，对象属性值
export function judgeTypeObj(obj) {
    for (const index of obj.data) {
        for (const key in index) {
            if (key === obj.attrName && obj.value === index[key]) {
                return index[obj.knowAttr]
            }
        }
    }
    return null
}

// 判断类型的方法  数组，对象属性，对象属性值
export function judgeType(arr, attrName, code) {
    for (const index of arr) {
        for (const key in index) {
            if (key === attrName) {
                return index[key]
            } else {
                return null
            }
        }
    }
}

// 判断金钱是否有小数点后两位
export function isMoney(num) {
    console.log(typeof num)
    const index = num.indexOf('.')
    let message = ''
    if (index === -1) {
        message = '价钱必须保留至小数点后两位'
    } else {
        const length = num.substr(index + 1).length
        if (length === 2) {
            message = null
        } else if (length === 1) {
            message = '价钱必须保留至小数点后两位'
        } else {
            message = '价钱最多保留至小数点后两位'
        }
    }
    return message
}