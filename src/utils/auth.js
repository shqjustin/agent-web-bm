import Cookies from 'js-cookie'

// const TokenKey = 'Admin-Token'
const TokenKey = 'token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getKeyValue(key) {
  return localStorage.getItem(key)
}

export function addParam(param) {
  const newParams = {
    'token': getToken(),
    'timeStamp': Date.parse(new Date())
  }
  if (param) {
    for (var key in param) {
      newParams[key] = param[key]
    }
  }
  return newParams
}

// export function getLocalValue(key) {
//   return localStorage.getItem(key)
// }

