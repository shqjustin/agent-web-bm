export function statusFomatter(isOk) {
  var codeMap = { '1': '启用', '0': '停用' }
  return codeMap[isOk]
}

export function timeSlice(time) {
  return time ? time.slice(0, 10) : ''
}

export function timeSliceSecond(time) {
  return time ? time.slice(0, 19) : ''
}
