import fetch from '@/utils/request'
// import { getToken } from '@/utils/auth'
import { addParams } from './common.js'

// 核销代付交易
export function modifyPaidList(params, data) {
    const uploadParams = addParams(params)
    return fetch({
        baseURL: '',
        url: '/api/allinpay.transfer.bill/pay/bill/updatepaywithholderror',
        method: 'post',
        data: data,
        params: uploadParams
    })
}

// 查询代扣交易列表
export function queryDeductedList(params, data) {
    const uploadParams = addParams(params)
    return fetch({
        baseURL: '',
        url: '/api/allinpay.transfer.bill/pay/bill/listwithholderrorno',
        method: 'post',
        data: data,
        params: uploadParams
    })
}