import request from '@/utils/request'
import { addParams } from './common.js'
import { getToken } from '@/utils/auth'

export function login(username, password) {
    return request({
        baseURL: '',
        url: '/api/allinpay.member.all/member/login',
        // url: '/user/login',
        method: 'post',
        params: {
            name: username,
            password: password
        }
    })
}

export function getInfo(token) {
    return request({
        url: '/user/info',
        method: 'get',
        params: { token }
    })
}

export function logout(token) {
    return request({
        // baseURL: 'https://www.easy-mock.com/mock/5a31e67a513048307be26eb9/mockService',
        // url: '/api/user/logout',
        url: '/api/allinpay.member.all/member/logout',
        method: 'post',
        params: { token }
    })
}

// 取菜单数据
export function getMenuData(params) {
    const uploadParams = {
        token: getToken(),
        menuBelong: 'xtbdbm',
        activeUrlId: 100000
    }
    return request({
        // baseURL: 'https://www.easy-mock.com/mock/5ab9d83305896018293276bf/bt',
        url: './static/api/menu.json',
        //url: '/api/allinpay.sysAdmin.rpmquery/rpm/listMenuOperations',
        method: 'get',
        params: uploadParams
    })
}