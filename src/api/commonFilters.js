// 把时间戳转换成一般的时间： 年-月-日 时：分：秒
export function transformTimeF(time) {
  const date = new Date(parseInt(time))
  const year = date.getFullYear()
  const month = date.getMonth() < 9 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1)
  const monthDay = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
  const hour = date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
  const minute = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
  const second = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
  return (year + '-' + month + '-' + monthDay + ' ' + hour + ':' + minute + ':' + second)
}

// 订单状态判断
export function orderStatusF(status) {
  const statusList = {
    '01': '申请',
    '02': '处理中',
    '03': '成功',
    '04': '失败',
    '05': '中止'
  }
  return statusList[status]
}
