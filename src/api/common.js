import { getToken } from '@/utils/auth'
// 给每个请求加上 token 和 activeUrlId
export function addParams(params) {
  const uploadParams = {
    'token': getToken()
    // 'activeUrlId': '1000000'
  }
  if (params) {
    for (const key in params) {
      uploadParams[key] = params[key]
    }
  }
  return uploadParams
}
// 给每个请求加上 token 和 activeUrlId
export function addHeaders(params) {
  return {
    tokenMemberId: localStorage.getItem('userId'),
    operatorName: localStorage.getItem('userName')
  }
}
// 深拷贝
export function deepCopy(obj) {
  const result = {}
  for (const key in obj) {
    result[key] = obj[key]
  }
  return result
}
// 清空之字符串的前后空格,中间的多个空格换成一个空格
export function TrimContent(str) {
  if (typeof (str) === 'string') {
    str = str.replace(/(^\s*)|(\s*$)/g, '')
    str = str.replace(/\s+/g, ' ')
  }
  return str
}
// 清空之字符串的前后空格
export function Trim(str) {
  if (typeof (str) === 'string') {
    str = str.replace(/(^\s*)|(\s*$)/g, '')
  }
  return str
}
// 清空之字符串的前后空格
export function myTrim(str) {
  if (typeof (str) === 'string') {
    str = str.replace(/(^\s*)|(\s*$)/g, '')
  }
  return str
}
// 清空对象中属性值为空的属性
export function clearNullAttr(obj) {
  for (const key in obj) {
    obj[key] = myTrim(obj[key])
    if (obj[key] === '' || obj[key] === undefined || obj[key] === null || obj[key].length === 0) {
      delete obj[key]
    } else {
      obj[key] = obj[key]
    }
  }
  return obj
}
// 清空弹出框,清空一个对象
export function clearObj(obj) {
  for (const key in obj) {
    obj[key] = null
  }
}
// 延时期 指令 指令值
export function delayer(directiveName, val, time) {
  setTimeout(() => {
    directiveName = val
  }, time)
}
// 关闭弹出框  （关闭弹出框指令,清空弹出框数据对象名字）
export function closeDialog(closeOrder, name) {
  setTimeout(() => {
    closeOrder = false
    clearDialog(name) // 清空弹出框
  }, 400)
}
// 时间撮转换为一般时间格式  （时间撮）
export function transformTime(time) {
  const date = new Date(time)
  const year = date.getFullYear()
  const month = date.getMonth() < 9 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1)
  const monthDay = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
  const hour = date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
  const minute = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
  const second = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
  return (year + '-' + month + '-' + monthDay + ' ' + hour + ':' + minute + ':' + second)
}
// 判断类型的方法  数组，对象属性，对象属性值
export function judgeTypeObj(obj) {
  for (const index of obj.data) {
    for (const key in index) {
      if (key === obj.attrName && obj.value === index[key]) {
        return index[obj.knowAttr]
      }
    }
  }
  return null
}

// 判断类型的方法  数组，对象属性，对象属性值
export function judgeType(arr, attrName, code) {
  for (const index of arr) {
    for (const key in index) {
      if (key === attrName) {
        return index[key]
      } else {
        return null
      }
    }
  }
}

// 判断金钱是否有小数点后两位
export function isMoney(num) {
  console.log(typeof num)
  const index = num.indexOf('.')
  let message = ''
  if (index === -1) {
    message = '价钱必须保留至小数点后两位'
  } else {
    const length = num.substr(index + 1).length
    if (length === 2) {
      message = null
    } else if (length === 1) {
      message = '价钱必须保留至小数点后两位'
    } else {
      message = '价钱最多保留至小数点后两位'
    }
  }
  return message
}
export function deepCopyArrObj(arr) { // 深度克隆数组对象
  const newarr = []
  for (let i = 0, len = arr.length; i < len; i++) {
    const source = arr[i]
    const deepCopy = function(source) {
      const result = {}
      for (const key in source) {
        // 状态转换
        result[key] = source[key]
        if (key === 'status') {
          if (source[key] === '1') {
            result['statusCd'] = true
          } else if (source[key] === '0') {
            result['statusCd'] = false
          }
        }
      }
      return newarr.push(result)
    }
    deepCopy(source)
  }
  console.log('------------------')
  console.log(newarr)
  return newarr
}
