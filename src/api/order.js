import fetch from '@/utils/request'
// import { getToken } from '@/utils/auth'
import { addParams } from './common.js'

// 查询订单列表  --sha
export function queryOrderList(params, data) {
    const uploadParams = addParams(params)
    return fetch({
        baseURL: '',
        url: '/api/allinpay.balance.service/balance/queryTransferOrderAll',
        method: 'post',
        data: data,
        params: uploadParams
    })
}