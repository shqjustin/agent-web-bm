import fetch from '@/utils/request'
// import { getToken } from '@/utils/auth'
import { addParams } from './common.js'


// 参数配置
export function queryConfig(params) {
    const uploadParams = addParams(params)
    return fetch({
        // baseURL: 'https://www.easy-mock.com/mock/5ab9d83305896018293276bf/bt',
        url: '/api/allinpay.balance.service/balance/getConfig',
        method: 'post',
        params: uploadParams
    })
}

// 参数配置 修改更新
export function updateConfig(params, data) {
    const uploadParams = addParams(params)
    return fetch({
        // baseURL: 'https://www.easy-mock.com/mock/5ab9d83305896018293276bf/bt',
        url: '/api/allinpay.balance.service/balance/updateConfig',
        method: 'post',
        params: uploadParams,
        data: data
    })
}